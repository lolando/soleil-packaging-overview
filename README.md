# SOLEIL-Packaging-Overview

|[**Synchrotron Soleil**](https://www.synchrotron-soleil.fr/en)|
|---|
|![Synchrotron SOLEIL](https://www.synchrotron-soleil.fr/sites/default/files/logo_0.png) |

This repository contains information about the packages we are pushing at [**Synchrotron Soleil**](https://www.synchrotron-soleil.fr/en). 
They complement the more global effort of the [Debian Science Team](https://wiki.debian.org/DebianScience/) and its [Salsa project](https://salsa.debian.org/science-team). We use the [pan-maintainers](https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-pan-maintainers) mailing list to keep track of our packaging efforts. You are welcome to register.

---

Table of content
1. [What is on-the-way](#current)
2. [Future packages: HIGH priority](#future-high)
3. [Future packages: Medium priority](#future-medium)
4. [Future packages: low priority](#future-low)
5. [Future packages: binary only](#future-binary)
6. [Already available](#done)

---

Rationale
---
To be able to help our numerous x-ray beam-lines, the SOLEIL Data Reduction and Analysis Group has adopted  a methodology centered on a coherent distribution of scientific software in repositories. 
The Debian system provides, to date, the most extensive scientific software package list. This allows a seamless installation and maintenance on our data treatment machines, 
as well as an easy configuration of any derived virtual machine, Docker container and more generally "web" micro-services.

<a name="current"></a>
What is on-the-way
---

| Package (upstream) | Category | Debian Link | Status    | Remarks                 |
|--------------------|----------|-------------|-----------|-------------------------|
| [dials](https://dials.github.io/)                     | Diffraction | https://salsa.debian.org/science-team/dials       | WIP  | [F. Picca](https://salsa.debian.org/users/picca), hard work |
| [xsocs](https://gitlab.esrf.fr/kmap/xsocs)            | Diffraction (surface) | https://salsa.debian.org/science-team/xsocs       | WIP  | [F. Picca](https://salsa.debian.org/users/picca) |
| [bioxtas-raw](https://bioxtas-raw.readthedocs.io/en/latest/) | SAXS | TBD | WIP, waiting for https://salsa.debian.org/alteholz/bitshuffle/-/merge_requests/1  | SAXS GUI | 
| [python3-AnyQt](https://pypi.org/project/AnyQt/) | GUI lib | https://salsa.debian.org/python-team/packages/python-anyqt  | WIP   | WIP [Freexian](https://www.freexian.com/) python lib GUI |
| [coot](https://www2.mrc-lmb.cam.ac.uk/personal/pemsley/coot/) | MX GUI  | https://salsa.debian.org/science-team/coot | WIP, DebianPAN: MX | Andrius Merkys |  |
| [crystfel](https://www.desy.de/~twhite/crystfel/)     | MX (FEL) | https://salsa.debian.org/science-team/crystfel | WIP, DebianPAN: MX  | not very active |
| [cctbx](https://cci.lbl.gov/cctbx_docs/index.html)    | MX | https://salsa.debian.org/science-team/cctbx      | DebianPAN: MX: | must update repo |
| [eiger2cbf](https://github.com/biochem-fan/eiger2cbf) | MX, I/O, HDF5 | https://salsa.debian.org/farhi-guest/eiger2cbf    | WIP   | [E. Farhi](https://salsa.debian.org/farhi-guest) |
| [looktxt](https://github.com/farhi/looktxt)           | I/O | https://salsa.debian.org/farhi-guest/looktxt  | WIP  | [E. Farhi](https://salsa.debian.org/farhi-guest) |
| [nbsphinx-link](https://pypi.org/project/nbsphinx-link/) | Notebook tool | https://salsa.debian.org/science-team/nbsphinx-link | WIP (in NEW) | [Freexian](https://www.freexian.com/) python lib for PyNX |
| [python3-openTSNE](https://pypi.org/project/openTSNE/) | ML |  https://salsa.debian.org/science-team/opentsne | WIP (in NEW)  | [Freexian](https://www.freexian.com/) python lib, for Orange3, highly used |
| [xraylib](https://github.com/tschoonj/xraylib) | a library for interactions of X-rays with matter | https://github.com/tschoonj/xraylib | WIP (in NEW) | [Freexian](https://www.freexian.com/) python lib, highly used |
| [epics-base](https://epics.anl.gov/index.php) | EPICS libraries | https://salsa.debian.org/science-team/epics-base | WIP (in NEW) | [Freexian](https://www.freexian.com/) needed by xraylarch |
| [python-pyepics](https://pyepics.github.io/pyepics/) | EPICS python bindings | https://salsa.debian.org/science-team/python-pyepics | WIP | [Freexian](https://www.freexian.com/) needed by xraylarch |
| [xraylarch](https://xraypy.github.io/xraylarch/)      | XANES/EXAFS spectroscopy | https://salsa.debian.org/science-team/xraylarch |  WIP by Freexian | needs a push for upstream; contains bin executables (FEFF): https://github.com/xraypy/xraylarch/issues/280 describes the license; ignore pyshortcuts dep: OK; needs xraydb from https://pypi.org/project/xraydb/ |
| jupyterhub | notebook account management | https://salsa.debian.org/python-team/packages/jupyterhub | huge WIP (30+ deps)  | by [lolando](https://salsa.debian.org/lolando) at GNURANDAL |
| [moviepy](https://pypi.org/project/moviepy/)  | video |  https://salsa.debian.org/python-team/packages/moviepy | WIP | for bcdi |
| [dioptas](http://www.clemensprescher.com/programs/dioptas) and [git](https://github.com/Dioptas/Dioptas) | powder diffraction | https://salsa.debian.org/python-team/packages/dioptas | WIP | GUI on top pf PyFAI |
| mantis   | spectro-microscopy | https://spectromicroscopy.com/| WIP | Absorption microscopy |
| [clpeak](https://github.com/krrishnarraj/clpeak)   | OpenCL |  https://salsa.debian.org/opencl-team/clpeak   | WIP (in NEW) | [Freexian](https://www.freexian.com/) Neil Williams |

<a name="future-high"></a>
Future packages we are considering: HIGH priority
---

| Package       | Category | Link | Priority/Status     | Remarks                    |
|---------------|----------|------|---------------------|----------------------------|
| XRSTools | inelastic  | https://github.com/christophsahle/XRStools | HIGH | X/Raman imaging GUI |
| navarp   | inelastic ARPES  | https://gitlab.com/fbisti/navarp| HIGH | ARPES GUI |
| pyobjcryst | diffraction | https://github.com/diffpy/pyobjcryst | HIGH | diff low level lib (FOX) |
| dmrg++   | ARPES | https://github.com/g1257/dmrgpp            | HIGH | theoretical |
| tomogui  | Tomo GUI | https://pypi.org/project/tomogui/       | HIGH | Tomo interface on top of PyHST2 |
| tomopy   |Tomo | https://tomopy.readthedocs.io/en/latest/     | HIGH     | DebianPAN: Tomography |
| [mumax3](https://mumax.github.io/) | micromagnetic simulation | https://github.com/mumax/3 | HIGH | |
| jana 2000 | Diffraction | http://jana.fzu.cz/                 | HIGH | diff powder, alt. for FullProf |
| fiji     | imaging GUI | https://fiji.sc/                     | HIGH |            ImageJ upgrade, could be complex |
| threeb   | Microscopy imaging | http://www.coxphysics.com/3b/index.html  | HIGH|  DebianPAN: microscopy, ImageJ plugin |
| tomoj    | Tomo | https://sourceforge.net/projects/tomoj/     | HIGH | ImageJ plugin |
| phonopy  | inelastic | https://phonopy.github.io/phonopy/     | HIGH | RIXS/XPS/ARPES |
| mosflm   | MX | https://www.mrc-lmb.cam.ac.uk/mosflm/mosflm/  | HIGH | MX simulation |
| freeart  | Tomo | https://gitlab.esrf.fr/freeart/freeart      | HIGH | DebianPAN: Tomo |
| [bcdi](https://pypi.org/project/bcdi/) | Bragg CDI | http://salsa.debian.org/science-team/bcdi | HIGH | Bragg CDI/ptycho, requires PyNX which is non-free |

<a name="future-medium"></a>
Future packages we are considering: Medium priority
---

| Package       | Category | Link | Priority/Status     | Remarks                    |
|---------------|----------|------|---------------------|----------------------------|
| pyxrd | Diffraction (powder) | https://github.com/PyXRD/PyXRD  |  Medium |  DebianPAN: Powder |
| mcstas | BL modelling | https://github.com/McStasMcXtrace/McCode | Medium |  BL modelling (neutrons) |
| mcxtrace | BL modelling| https://github.com/McStasMcXtrace/McCode| Medium | BL modelling (X) |
| refmac5 | MX | [refmac tgz](https://www2.mrc-lmb.cam.ac.uk/groups/murshudov/content/refmac/SourceEtal/refmac5.8_v0091.tgz) | Medium | |
| mdanse (ex nMoldyn) | Spectroscopy |https://code.ill.fr/scientific-software/mdanse | Medium |  MD to S(q,w) and more |
| xdsme | MX | https://github.com/legrandp/xdsme | Medium |  DebianPAN: MX |
| mxcube | MX | https://github.com/mxcube/mxcube | Medium |  DebianPAN: MX |
| quasar/orange3 | spectroscopy GUI | https://github.com/Quasars/orange-spectroscopy | Medium | with spectroscopy IR/UV |
| arpes | ARPES | https://github.com/chstan/arpes | Medium | extensive, runs from Jupyter NB, lots of unpackaged dependencies |
| mpes | XPS/ARPES | https://github.com/mpes-kit/mpes | Medium, WIP, lots of unpackaged dependencies | work by [Freexian](https://www.freexian.com/) |
| [bokeh](https://github.com/bokeh/bokeh) | lib Jupyter plots | TBD | WIP, unpackaged nodejs dependencies are a problem | work by [Freexian](https://www.freexian.com/) |

<a name="future-low"></a>
Future packages we are considering: low priority
---

| Package       | Category | Link | Priority/Status     | Remarks                    |
|---------------|----------|------|---------------------|----------------------------|
| gsas2 | Diffraction(powder) | https://subversion.xray.aps.anl.gov/trac/pyGSAS and https://salsa.debian.org/science-team/gsas-ii  | Low |  PyGSAS aka GSAS-II partly ported to Debian |
| cif2hkl | Diffraction | http://www.mcstas.org/download/share/           | Low  | Compute F^2 for neutrons, X and electrons, based on FullProf/CrysFML. Code in Fortran |
| nebula | Diffraction |https://github.com/AndreasReiten/nebula          | Low  | **Untouched/5y**  DebianPAN: Diffraction?|
| crysfml | Diffraction | https://code.ill.fr/scientific-software/crysfml | Low | Fortran Library |
| adxv | MX | https://www.scripps.edu/tainer/arvai/adxv.html | Low | no src |
| HipGISAXS | SAXS | https://github.com/HipGISAXS/HipGISAXS | Low | 3yrs old |
| bonsu | CDI | https://github.com/bonsudev/bonsu                 | Low |  DebianPAN: Coherent-diffraction |
| hawk  |CDI |http://xray.bmc.uu.se/hawk/?q=hawk/whatishawk     | Low|  DebianPAN: Coherent-diffraction |
| tomwer | Tomo/Orange3 | https://gitlab.esrf.fr/tomotools/tomwer | Low | requires Orange3 |
| demeter | EXAFS | https://github.com/bruceravel/demeter | Low | requires ifeffit or larch or FEFF |
| phon | spectroscopy (vibrational)  | http://www.homepages.ucl.ac.uk/~ucfbdxa/phon/      | Low |  A program to calculate phonons using the small displacement method |
| rescal | neutron spectroscopy | [rescal doc/src](http://ifit.mccode.org/Applications/ResLibCal/doc/ResLibCal.html) | Low  | Legacy Neutron TAS resolution calculation, simple Fortran code |
| spinwave | spin-wave spectroscopy | http://www-llb.cea.fr/logicielsllb/SpinWave/SW.html | Low |  Compute spin-wave dispersions (CEA/LLB), in fortran |
| spectra | Source modelling| http://spectrax.org/spectra/ | Low | source sim |
| simplex | Source modelling| http://spectrax.org/simplex/index.html |Low  | source sim |
| genesis| Source modelling | http://genesis.web.psi.ch/  | Low |  source sim |
| oasys/orange3 | BL modelling | https://github.com/oasys-kit | Low | source and BL modelling |
| srw | BL modelling | https://github.com/ochubar/SRW | Low | source sim |
| simex | BL modelling | https://github.com/PaNOSC-ViNYL/SimEx | Low | XFEL simulation |
| mlfsom | MX modelling | https://bl831.als.lbl.gov/~jamesh/mlfsom/ | Low | MX simulation |
| idl2matlab | misc (converter) | https://github.com/farhi/idl2matlab | Low |  Convert IDL code into Matlab (flex/bison). Can be tricky (old C) |
| python3-extranormal3 | XANES/EXAFS |https://pypi.org/project/extranormal3/     | Low |  Postponed, python lib |

<a name="future-binary"></a>
Software that can only be distributed as binary (contrib/non-free)
---

| Package       | Category | Link | Priority/Status     | Remarks                    |
|---------------|----------|------|---------------------|----------------------------|
| MAUD | Diffraction | http://maud.radiographema.eu/ | Low | |
| FullProf | Diffraction | https://www.ill.eu/sites/fullprof/ | Low | based on CrysFML, no source code avail. |
| ANA-ROD | Diffraction (surface) | http://www.esrf.eu/computing/scientific/joint_projects/ANA-ROD/ | Low | can not distribute src. Use Numerical recipies. |
| pyROD | Diffraction (surface) | http://zhoutao.eu/pyrod/ | Low | deprecated dependencies (gui) |
| ATSAS | SAXS | https://www.embl-hamburg.de/biosaxs/download.html | Low | need registration |
| ShelX | MX | http://shelx.uni-goettingen.de/ | Low | need registration |
| fit2d | Diffraction | https://gitlab.esrf.fr/hammersl/fit2d | Low | old but still highly used |
| pynx | ptychography | http://www.esrf.eu/computing/scientific/PyNX/README.html | Low | issue with license/patent |
| fdmnes | EXAFS | http://neel.cnrs.fr/spip.php?article3137 | Low | no src |
| feff | EXAFS | http://feff.phys.washington.edu/feffproject-feff-download.html | Low | license/commercial  FEFF8 http://leonardo.phys.washington.edu/feff/|

<a name="done"></a>
Other packages we are happy to see in Debian
---

| Package       | Category | Debian Link |  Remarks                    |
|---------------|----------|-------------|-----------------------------|
| [python3-spectral](https://github.com/spectralpython/spectral/) | multi-spectral | https://salsa.debian.org/science-team/python-spectral in [SID](https://tracker.debian.org/pkg/python-spectral) | [Freexian](https://www.freexian.com/) python lib, for Orange3 |
| [python3-pynndescent](https://github.com/lmcinnes/pynndescent) | optimization | https://salsa.debian.org/med-team/python-pynndescent.git in [SID](https://packages.debian.org/source/testing/python-pynndescent) | Debian-Med python lib |
| [python3-louvain](https://github.com/taynaud/python-louvain) | graphs  | https://salsa.debian.org/python-team/modules/python-louvain  in [SID](https://packages.debian.org/source/sid/python-louvain) | for Orange3, needs update ? |
| [qemu-remote-desktop](https://gitlab.com/soleil-data-treatment/soleil-software-projects/remote-desktop) | Virtual Data treatment | https://salsa.debian.org/debian/qemu-web-desktop | in [NEW](https://ftp-master.debian.org/new/qemu-web-desktop_21.04.10-2.html) | Pushed by [R. Mas](https://salsa.debian.org/lolando)  |
| [pyhst2](http://ftp.esrf.fr/scisoft/PYHST2/)          | Tomo | https://salsa.debian.org/science-team/pyhst2      | in [SID](https://packages.debian.org/sid/python3-pyhst2-cuda)    | pushed by [Freexian](https://www.freexian.com/)     |
| [xrayutilities](https://xrayutilities.sourceforge.io/) | Diffraction | https://salsa.debian.org/freexian-team/xrayutilities | in [SID](https://packages.debian.org/sid/xrayutilities)  | pushed by [Freexian](https://www.freexian.com/), using previous repo from [F. Picca](https://salsa.debian.org/users/picca) |
| [astra-toolbox](https://www.astra-toolbox.com/)       | Tomo  | https://salsa.debian.org/science-team/astra-toolbox | in [SID](https://packages.debian.org/sid/python3-astra-toolbox)  | pushed by [Freexian](https://www.freexian.com/)     |
| [python3-procrunner](https://pypi.org/project/procrunner) | system | https://salsa.debian.org/science-team/python-procrunner | in [SID](https://packages.debian.org/sid/python3-procrunner) | pushed by [Freexian](https://www.freexian.com/) |
| [pyct](https://github.com/pyviz-dev/pyct/)        |  | https://salsa.debian.org/science-team/pyct  | in [SID](https://packages.debian.org/sid/python3-pyct)  | pushed by [Freexian](https://www.freexian.com/)
| [python3-colorcet](https://github.com/holoviz/colorcet) | color lib | https://salsa.debian.org/science-team/colorcet           | in [SID](https://tracker.debian.org/pkg/colorcet)  | pushed by [Freexian](https://www.freexian.com/) |
| [python3-tifffile](https://pypi.org/project/tifffile/)  | I/O | https://salsa.debian.org/python-team/packages/tifffile | in [SID](https://packages.debian.org/sid/python3-tifffile) | python lib, for HypersPy |
| [dials-data](https://pypi.org/project/dials-data/ ) | data for DIALS | https://salsa.debian.org/science-team/dials-data | in [SID](https://packages.debian.org/sid/dial-data) | python lib, for DIALS, pushed by [Freexian](https://www.freexian.com/) |
| [hyperspy](https://hyperspy.org/) | multi-spectral | https://salsa.debian.org/science-team/hyperspy                          | in [SID](https://tracker.debian.org/pkg/hyperspy)     | pushed by [Freexian](https://www.freexian.com/) |
| [denss](https://github.com/tdgrant1/denss.git) | SAXS | https://salsa.debian.org/science-team/denss | in [SID](https://tracker.debian.org/pkg/denss) |  pushed by [Freexian](https://www.freexian.com) |
| [genx](https://genx.sourceforge.io/)  | Reflectvity | https://salsa.debian.org/science-team/genx | in [SID](https://tracker.debian.org/pkg/genx) |   pushed by [Freexian](https://www.freexian.com) |
| [bornagain](https://www.bornagainproject.org/)        | Reflectivity | https://salsa.debian.org/science-team/bornagain  | in [SID](https://packages.debian.org/sid/bornagain), python3.8 bindings not functional  | pushed by [Freexian](https://www.freexian.com/) |
| [igor](http://git.tremily.us/?p=igor) | XPS/ARPES | https://salsa.debian.org/science-team/igor  | in [SID](https://tracker.debian.org/pkg/python-igor) | pushed by [Freexian](https://www.freexian.com/) |
| [arpys](https://github.com/kuadrat/arpys) | XPS/ARPES | https://salsa.debian.org/science-team/arpys  | in [SID](https://tracker.debian.org/pkg/arpys) | work by [Freexian](https://www.freexian.com/) |
| binoculars    | Diffraction (surface)  | https://salsa.debian.org/science-team/binoculars https://packages.debian.org/source/buster/binoculars | DebianPAN: 3D Diffraction |
| bitshuffle    | compression| https://salsa.debian.org/alteholz/bitshuffle  https://packages.debian.org/source/sid/bitshuffle   | |
| [cbflib](http://www.bernstein-plus-sons.com/software/CBF/)  https://packages.debian.org/source/buster/cbflib  | MX | https://salsa.debian.org/science-team/cbflib      |  [F. Picca](https://salsa.debian.org/users/picca) contribution |
| fityk         | Fitting GUI |https://salsa.debian.org/science-team/fityk  https://packages.debian.org/source/buster/fityk   | DebianPAN: Data-reduction |
| ghkl          | Diffraction| https://salsa.debian.org/science-team/hkl       | DebianPAN: control-systems |
| gwyddion      | Imaging Microscopy |https://salsa.debian.org/med-team/gwyddion   https://packages.debian.org/buster/gwyddion   | DebianPAN: Microscopy |
| h5py          | I/O HDF5 |https://salsa.debian.org/science-team/h5py   https://packages.debian.org/buster/python3-h5py    | Data-reduction |
| horae         | EXAFS | https://packages.debian.org/buster/horae  |  Athena Artemis  Hephaestus |
| imagej        | Imaging GUI |https://salsa.debian.org/med-team/imagej    https://packages.debian.org/buster/imagej     | DebianPAN: Data-reduction |
| itango        | Control |https://salsa.debian.org/science-team/itango  https://packages.debian.org/buster/python-itango  | DebianPAN: control-systems |
| labplot       | Origin clone | https://packages.debian.org/buster/labplot | Data analysis / Origin |
| libxy         | Diffraction (powder) | https://packages.debian.org/source/buster/xylib | DebianPAN: Powder |
| lz4           | compression| https://salsa.debian.org/debian/lz4  https://packages.debian.org/buster/lz4    | |
| mayavi2       | 3D Viz | https://packages.debian.org/buster/mayavi2 | DebianPAN: 3D :warning: obsolete ? |
| objcryst-fox  | Diffraction| https://packages.debian.org/sid/amd64/objcryst-fox | :warning: obsolete ? |
| octave        | Matlab-clone|https://salsa.debian.org/pkg-octave-team/octave https://packages.debian.org/buster/octave| DebianPAN: Data-reduction |
| paraview      | 3D Viz | https://salsa.debian.org/science-team/paraview https://packages.debian.org/buster/paraview  | DebianPAN: 3D |
| pyfai         | Diffraction (powder/SAXS) | https://salsa.debian.org/science-team/pyfai  https://packages.debian.org/buster/pyfai   | DebianPAN: Powder Diffraction |
| pymca         | Fluo/Imaging|https://salsa.debian.org/science-team/pymca   https://packages.debian.org/buster/pymca  | DebianPAN: Data-reduction |
| [python3-PTable](https://pypi.org/project/PTable/) | I/O | https://packages.debian.org/source/buster/ptable |  python lib, for HypersPy |
| pytango       | Control |https://salsa.debian.org/science-team/pytango  https://packages.debian.org/buster/python-pytango  | DebianPAN: control-systems |
| [Stable/python3-yaml / PyYAML](https://pypi.org/project/PyYAML/) | I/O | https://packages.debian.org/source/buster/pyyaml | python lib, for HypersPy. |
| [sardana](https://github.com/sardana-org/sardana)   https://packages.debian.org/buster/python-sardana    | Control | https://salsa.debian.org/science-team/sardana     |  [F. Picca](https://salsa.debian.org/users/picca), but we are not using it at Soleil |
| sasview       | SAXS/SANS |https://salsa.debian.org/science-team/sasview  https://packages.debian.org/buster/sasview  | DebianPAN: Powder Diffraction (SAS) |
| silx          | Imaging, GUI lib|https://salsa.debian.org/science-team/silx   https://packages.debian.org/buster/silx    | DebianPAN: Data-reduction |
| spd           | Diffraction (powder) | https://packages.debian.org/source/buster/spd   | DebianPAN: Powder Diffraction, superseeded by pyfai |
| skimage       | Imaging | https://salsa.debian.org/science-team/skimage  https://packages.debian.org/buster/python3-skimage  | DebianPAN: Microscopy |
| tango         | Control |https://salsa.debian.org/science-team/tango  https://packages.debian.org/buster/tango-test   | DebianPAN: control-systems |
| [taurus](https://github.com/taurus-org/taurus)    https://packages.debian.org/buster/python-taurus    | Control GUI | https://salsa.debian.org/science-team/taurus      |  [F. Picca](https://salsa.debian.org/users/picca), but we are not using it at Soleil |
| [taurus-pyqtgraph](https://github.com/taurus-org/taurus_pyqtgraph) https://packages.debian.org/sid/python3-taurus-pyqtgraph | Control GUI| https://salsa.debian.org/science-team/taurus    |  [F. Picca](https://salsa.debian.org/users/picca), but we are not using it at Soleil |
| ufo-core      | Tomo | https://salsa.debian.org/science-team/ufo-core https://packages.debian.org/buster/libufo-data  | DebianPAN: Tomography |
| ufo-filters   | Tomo | https://salsa.debian.org/science-team/ufo-filters https://packages.debian.org/buster/ufo-filters | DebianPAN: Tomography |
| veusz         | 3D Viz | https://packages.debian.org/sid/veusz | 3D |
| vistrails     | GUI | https://salsa.debian.org/science-team/vistrails https://packages.debian.org/buster/vistrails | DebianPAN: Data-reduction :warning: STOPPED project |


